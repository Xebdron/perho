package com.perho.web.controller;

import com.perho.web.dto.TestInfo;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Xebdron on 2/5/2017.
 */

@RestController
public class HelloController {

    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public ResponseEntity<TestInfo> printWelcome() {
        TestInfo testInfo = new TestInfo();
        testInfo.setMessage("nice bby");

        return new ResponseEntity(testInfo, HttpStatus.OK);
    }
}
