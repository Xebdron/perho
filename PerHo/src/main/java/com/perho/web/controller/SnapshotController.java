package com.perho.web.controller;

import com.perho.web.dto.SnapshotInfo;
import com.perho.web.services.SnapshotService;
import com.sun.org.apache.xml.internal.security.exceptions.Base64DecodingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

/**
 * Created by Xebdron on 2/12/2017.
 */
@RestController
@RequestMapping(value = "/snapshot")
public class SnapshotController {
    @Autowired
    private SnapshotService snapshotService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<SnapshotController>> getSnapshots(@RequestParam List<String> requests) {
        return new ResponseEntity(snapshotService.getSnapshots(requests), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public void saveSnapshots(@RequestBody List<SnapshotInfo> snapshotInfos) throws Base64DecodingException, IOException {
        snapshotService.saveSnapshots(snapshotInfos);
    }
}
