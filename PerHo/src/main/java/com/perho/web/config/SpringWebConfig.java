package com.perho.web.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Created by Xebdron on 2/5/2017.
 */
@EnableWebMvc
@Configuration
@ComponentScan({"com.perho.web"})
public class SpringWebConfig extends WebMvcConfigurerAdapter{
}
