package com.perho.web.config;

import org.springframework.core.io.support.ResourcePropertySource;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import java.io.IOException;

/**
 * Created by Xebdron on 2/5/2017.
 */
public class SpringWebInitalizer implements WebApplicationInitializer{
    public void onStartup(ServletContext servletContext) throws ServletException {
        AnnotationConfigWebApplicationContext ctx = new AnnotationConfigWebApplicationContext();

        ctx.scan("com.perho.web.config");

//        try {
//            ctx.getEnvironment().getPropertySources().addFirst(new ResourcePropertySource("classpath:application.properties"));
//        } catch (IOException e) {
//            throw new ServletException(e);
//        }

        servletContext.addListener(new ContextLoaderListener(ctx));

        ServletRegistration.Dynamic registration = servletContext.addServlet("dispatcher", new DispatcherServlet(ctx));
        registration.setLoadOnStartup(1);
        registration.addMapping("/server/*");
    }
}
