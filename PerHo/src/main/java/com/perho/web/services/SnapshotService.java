package com.perho.web.services;

import com.perho.web.dto.SnapshotInfo;
import com.sun.org.apache.xml.internal.security.exceptions.Base64DecodingException;

import java.io.IOException;
import java.util.List;

/**
 * Created by Xebdron on 2/12/2017.
 */
public interface SnapshotService {
    public List<SnapshotInfo> getSnapshots(List<String> request);

    public void saveSnapshots(List<SnapshotInfo> snapshotInfos)throws IOException;
}
