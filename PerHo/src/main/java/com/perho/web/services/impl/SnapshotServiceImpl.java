package com.perho.web.services.impl;

import com.perho.web.dto.SnapshotInfo;
import com.perho.web.services.SnapshotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.ServletContext;
import javax.xml.bind.DatatypeConverter;
import java.io.*;
import java.nio.file.FileSystemException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Xebdron on 2/12/2017.
 */
@Service
public class SnapshotServiceImpl implements SnapshotService {
    @Autowired
    ServletContext servletContext;

    public List<SnapshotInfo> getSnapshots(List<String> requests) {
        List<SnapshotInfo> snapshotInfos = new ArrayList<SnapshotInfo>();
        for (String request : requests){
            SnapshotInfo info = new SnapshotInfo();
            String directory = servletContext.getRealPath("/") + "\\resources\\images\\" + request + ".jpg";

            info.setCode(request);
            try {
                File file = new File(directory);

                FileInputStream imageInFile = new FileInputStream(file);
                byte imageData[] = new byte[(int) file.length()];
                imageInFile.read(imageData);

                imageInFile.close();

                info.setImageString(DatatypeConverter.printBase64Binary(imageData));
            } catch (IOException ioe){
                info.setImageExists(Boolean.FALSE);
            }
            snapshotInfos.add(info);

        }

        return snapshotInfos;
    }

    public void saveSnapshots(List<SnapshotInfo> snapshotInfos) throws IOException {
        for (SnapshotInfo info : snapshotInfos){
            String directory = servletContext.getRealPath("/") + "\\resources\\images\\" + info.getCode() + ".jpg";

            File file = new File(directory);
            if (!file.exists()){
                file.createNewFile();
            } else {
                continue;
            }

            FileOutputStream imageOutStream = new FileOutputStream(file);

            imageOutStream.write(DatatypeConverter.parseBase64Binary(info.getImageString().split(",")[1]));

            imageOutStream.close();
        }
    }
}
