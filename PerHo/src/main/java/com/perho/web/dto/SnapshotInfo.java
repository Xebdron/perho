package com.perho.web.dto;

/**
 * Created by Xebdron on 2/12/2017.
 */
public class SnapshotInfo {
    private String url;
    private String code;
    private byte[] datauri;
    private boolean imageExists = Boolean.TRUE;
    private String imageString;

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public byte[] getDatauri() {
            return datauri;
        }

        public void setDatauri(byte[] datauri) {
            this.datauri = datauri;
        }

    public boolean isImageExists() {
        return imageExists;
    }

    public void setImageExists(boolean imageExists) {
        this.imageExists = imageExists;
    }

    public String getImageString() {
        return imageString;
    }

    public void setImageString(String imageString) {
        this.imageString = imageString;
    }
}
