package com.perho.web.dto;

/**
 * Created by Xebdron on 2/5/2017.
 */
public class TestInfo {
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
