'use strict';

angular
    .module('core')
    .factory('BookmarksService', ['$resource', function($resource) {
        return $resource('resources/snapshots.json', {}, {
            query : {method : 'GET', isArray: true}
        });
    }]);