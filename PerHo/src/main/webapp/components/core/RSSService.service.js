'use strict';

angular.module('core')
    .factory('RSSService', ['$http', '$sce', function($http, $sce){
        return {
            getFeed: function(url) {
                $sce.trustAsResourceUrl(url);

                return $http.get("https://api.rss2json.com/v1/api.json",
                    { params: { "rss_url": url } }
                );
            }
        }
    }]);