'use strict';

angular
    .module('core')
    .factory('NumUtil', function() {
       return {
           intToStringFormat: function(input, length) {
               var output = '' + input;

               while (output.length < length) {
                   output = '0' + output;
               }

               return output;
           }
       }
    });