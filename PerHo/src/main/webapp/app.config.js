'use strict';

angular
    .module('perhoApp')
    .config(['$locationProvider', '$routeProvider',
        function config($locationProvider, $routeProvider){
            console.log('asd');
            $locationProvider.hashPrefix('!');

            $routeProvider
                .when('/home', {
                    templateUrl: 'components/home/home.html',
                    controller: ['$routeParams', '$scope', '$interval', '$http', '$base64', 'bookmarks', 'NumUtil', 'RSSService', 'ScreenshotService', 'BookmarksService',
                        function HomeController($routeParams, $scope, $interval, $http, $base64, bookmarks, NumUtil, RSSService, ScreenshotService, BookmarksService){

                            $scope.tempVar = {};
                            $scope.tempVar.countdownMsg = '';
                            $scope.tempVar.mainNews = [];
                            $scope.tempVar.worldNews = [];
                            $scope.tempVar.images = [];
                            $scope.tempVar.bookmarks = bookmarks;

                            var targetDate = new Date('07/02/2022');
                            var newsFeedUrl = 'http://www.philstar.com/rss/breakingnews/';
                            var worldNewsFeedUrl = 'http://www.philstar.com/rss/world';

                            function daysInMonth(month,year) {
                                return new Date(year, month, 0).getDate();
                            }

                            function getCountdown() {
                                var todayDate = new Date();
                                var carryOver = 0;

                                var secondDiff = targetDate.getSeconds() - todayDate.getSeconds();
                                if (secondDiff < 0){
                                    secondDiff = secondDiff + 60;
                                    carryOver = 1;
                                } else {
                                    carryOver = 0;
                                }

                                var minutesDiff = targetDate.getMinutes() - todayDate.getMinutes() - carryOver;
                                if (minutesDiff < 0){
                                    carryOver = Math.ceil((minutesDiff * -1) / 60);
                                    minutesDiff = minutesDiff + (60 * carryOver);
                                } else {
                                    carryOver = 0;
                                }


                                var hourDiff = targetDate.getHours() - todayDate.getHours() - carryOver;
                                if (hourDiff < 0){
                                    carryOver = Math.ceil((hourDiff * -1) / 60);
                                    hourDiff = hourDiff + (60 * carryOver);
                                } else {
                                    carryOver = 0;
                                }

                                var dayDiff = targetDate.getDate() - todayDate.getDate() - carryOver;
                                if (dayDiff < 0){
                                    carryOver = 1;

                                    var month = (todayDate == 1 ? 12 : todayDate.getMonth() - 1);
                                    var fullYear = todayDate.getFullYear();
                                    var year = (month == 12 ? fullYear - 1 : fullYear);
                                    dayDiff = dayDiff + daysInMonth(year, month);
                                } else {
                                    carryOver = 0;
                                }

                                var monthDiff = targetDate.getMonth() - todayDate.getMonth() - carryOver;
                                if (monthDiff <= 0){
                                    carryOver = 1;
                                    monthDiff = monthDiff + 12;
                                } else {
                                    carryOver = 0;
                                }

                                var yearDiff = targetDate.getFullYear() - todayDate.getFullYear() - carryOver;

                                $scope.tempVar.countdownMsg = NumUtil.intToStringFormat(yearDiff, 2) + ':'
                                    + NumUtil.intToStringFormat(monthDiff, 2) + ':'
                                    + NumUtil.intToStringFormat(dayDiff, 2) + ' '
                                    + NumUtil.intToStringFormat(hourDiff, 2) + ':'
                                    + NumUtil.intToStringFormat(minutesDiff, 2) + ':'
                                    + NumUtil.intToStringFormat(secondDiff, 2);
                            }

                            $interval(getCountdown, 1000);

                            function getNewsFeed(){
                                RSSService.getFeed(newsFeedUrl).then(function(res){
                                    $scope.tempVar.mainNews = res.data.items;
                                });
                            }

                            function getWorldNewsFeed() {
                                RSSService.getFeed(worldNewsFeedUrl).then(function(res){
                                    $scope.tempVar.worldNews = res.data.items;
                                });
                            }

                            getNewsFeed();
                            getWorldNewsFeed();
                            $interval(getNewsFeed, 15 * 60 * 1000);
                            $interval(getWorldNewsFeed, 15 * 60 * 1000);

                            $scope.loadToBookmarks = function (data) {
                                var allImagesReceived = true;
                                for (var ctr = 0; ctr < bookmarks.length; ctr++){
                                    if (bookmarks[ctr].url === data.url){
                                        bookmarks[ctr].imageString = data.datauri;
                                        bookmarks[ctr].imageExists = true;
                                    }
                                    if (!bookmarks[ctr].imageExists && bookmarks[ctr].code) {
                                        allImagesReceived = false;
                                    }
                                }

                                if (allImagesReceived) {
                                    $http.post('/server/snapshot', bookmarks, {}).then(function (result) {
                                        $scope.tempVar.bookmarks = bookmarks;
                                    });
                                }
                            };

                            var request = {};
                            request.bookmarks = [];
                            var ctr = 0;
                            for (var item in bookmarks){
                                if (item && bookmarks[ctr] && bookmarks[ctr].code){
                                    request.bookmarks.push(bookmarks[ctr].code);
                                }
                                ctr++;
                            }

                            $http.get('/server/snapshot', {params : {requests : request.bookmarks}}).success(function (result) {
                                var ctr1 = 0;
                                for (ctr1; ctr1 < result.length; ctr1++){
                                    bookmarks[ctr1].imageExists = result[ctr1].imageExists;
                                    if (!bookmarks[ctr1].imageExists){
                                        ScreenshotService.getScreenAndDisplayToId(bookmarks[ctr1].url, function (datauri, url) {
                                            $scope.loadToBookmarks({
                                                'datauri': datauri,
                                                'url': url
                                            });
                                        });
                                    } else {
                                        bookmarks[ctr1].imageString = 'data:image/jpeg;base64,' + result[ctr1].imageString.trim();
                                    }
                                }
                            });
                        }],
                    resolve : {
                        bookmarks : ['BookmarksService', function (BookmarksService) {
                            return BookmarksService.query().$promise.then(function (result) {
                                return result;
                            });
                        }]
                    }
                })
                .otherwise('/home');
        }
    ]);