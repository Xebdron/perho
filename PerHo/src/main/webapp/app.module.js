'use strict';

angular.module('perhoApp', [
   'ngRoute',
   'ngResource',
   'home',
   'core',
   'base64'
]);